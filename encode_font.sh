#!/usr/bin/env sh

# Use this script to get a base64 string representing a font file for embedding
# into the HTML.

set -euo pipefail

input="$1"
base64_output="base64_$(basename "$1")"
output="final_base64_$(basename "$1")"

base64 $1 > "$base64_output"
awk '{ printf "%s", $0 }' "$base64_output" > "$output"
