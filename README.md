# Resume
A simple resume template.

## Usage
```bash
./compile-resume.sh
```

## Requirements
[wkhtmltopdf](http://wkhtmltopdf.org/ "wkhtmltopdf")

